<?php

//how to select the leaderboard\\
//SELECT leaderboard_entries.ID, a.scoreTypeID as ScoreTypeA, a.score as ScoreA, b.scoreTypeID as ScoreTypeb, b.score as ScoreB FROM leaderboard_entries 
//LEFT JOIN `leaderboard_entries` as a ON `leaderboard_entries`.`ID` = `a`.`eventID` 
//AND (a.scoreTypeID = 2) 
//LEFT JOIN `leaderboard_entries` as b ON `leaderboard_entries`.`ID` = `b`.`eventID` 
//AND (b.scoreTypeID = 1)

//to generate the above::

//user something to loop through score type of a game and if theres 1 only generate A, if 2 generate B as Well etc..
//remember to generate scoretype order a, b, c etc...


//if(is_array($entries) && $_GET['add'] != '1' && $_GET['edit'] == ''){
$db = \Database::connection();


use Concrete\Core\Search\ItemList\Database;


if($_GET['edit'] != ''){
$editsearch = $db->query("SELECT * FROM `leaderboard_entries` WHERE ID = ?", array($_GET['edit']));
$edit = $editsearch->fetch();

$titletext = 'Edit';
}else{
$titletext = 'Add';    
}



if($_GET['add'] != '1' && $_GET['edit'] == ''){
?>    
<a href='?add=1&step=1' class='btn btn-primary'>Add a new Entry</a>
<br/><br/>
<input class="on-page-search" placeholder="Type Here To Highlight Values..."></input>
 <script type='text/javascript'>
jQuery(document).ready(function($) {
$(".on-page-search").on("keyup", function () {
var v = $(this).val();
$(".results").removeClass("results");
$(".ccm-search-results-table tr td").each(function () {
if (v != "" && $(this).text().search(new RegExp(v,'gi')) != -1) {
$(this).addClass("results");
}
});
});
}); </script>
 
 <style>
   /* Style the input */
.on-page-search {
width: 100%;
font-size: 14px;
line-height: 26px;
color: #787d85;
background-color: #fcfcfc;
border: 1px solid #e0e1e1;
padding: 5px 15px;
}

/* Style the list */
.demo-links {
border-bottom: none;
padding: 5px 5px;
line-height: 36px;
}

/* Style the results */
.results {
background: #de1919 !important;
color: white !important;
}
.results:hover {
background: #333 !important;
color: white !important;
}
 </style>
       
<table border="0" cellspacing="0" cellpadding="0" class="ccm-search-results-table">
	<thead>
            <tr>
                <th class="">User Full Name / Email</th>
                <th class="">Event</th>
                <th class="">Game</th>
                <th class="">Scores</th>
                <th class="">Actions</th>
            </tr>
        </thead>
	<tbody>

<?php  
if(is_array($leaderboard)){
foreach($leaderboard as $entry){
  
  if($ui = UserInfo::getByID($entry['userID'])){
  $fName = trim(h($ui->getAttribute('first_name')));
  $lName = trim(h($ui->getAttribute('last_name')));
  $email = $ui->getUserEmail();
  
  //find event name from ID
  $eventNameSearch = $db->query("SELECT eventName FROM leaderboard_leaderboard_events WHERE ID = ?", array($entry['eventID']));
  $event = $eventNameSearch->fetch();
  $eventName = $event['eventName'];
  
  //find game name from ID
  $gameNameSearch = $db->query("SELECT gameTitle FROM leaderboard_games WHERE ID = ?", array($entry['gameID']));
  $game = $gameNameSearch->fetch();
  $gameName = $game['gameTitle'];
  
  //find score name A from ID
  $scoreNameASearch = $db->query("SELECT typeName FROM leaderboard_game_score_types WHERE ID = ?", array($entry['ScoreTypeA']));
  $scoreNameA = $scoreNameASearch->fetch();
  $scoreAName = $scoreNameA['typeName'];
  $scoreA = $entry['ScoreA'];
  
  //find score name B from ID
  $scoreNameBSearch = $db->query("SELECT typeName FROM leaderboard_game_score_types WHERE ID = ?", array($entry['ScoreTypeb']));
  $scoreNameB = $scoreNameBSearch->fetch();
  $scoreBName = $scoreNameB['typeName'];
  $scoreB = $entry['ScoreB'];
  
  //find score name C from ID
  $scoreNameCSearch = $db->query("SELECT typeName FROM leaderboard_game_score_types WHERE ID = ?", array($entry['ScoreTypec']));
  $scoreNameC = $scoreNameCSearch->fetch();
  $scoreCName = $scoreNameC['typeName'];
  $scoreC = $entry['ScoreC'];
    echo "<tr>";
    echo "<td>" . $fName . ' ' . $lName . ' - '. $email;
    echo "</td>";
    echo "<td>" . $eventName;
    echo "</td>";
    echo "<td>" . $gameName;
    echo "</td>";
    echo "<td>";
    
    if($scoreAName != ''){
    echo $scoreAName . ' - ' . $scoreA . '<br/>';
    }
    
    if($scoreBName != ''){
    echo $scoreBName . ' - ' . $scoreB . '<br/>';
    }
    
    if($scoreCName != ''){
    echo $scoreCName . ' - ' . $scoreC . '<br/>';
    }
    
    echo "</td>";
    echo "<td>" . "<a href='?edit=".$entry['ENTRYID'] ."&step=1' class='btn btn-primary'>Edit</a>";
    echo "<a style='margin-left:15px;' href='". $this->action('delete', $entry['ENTRYID']) ."' class='btn btn-danger'>Delete</a>";
    echo "</td>";
    echo "</tr>";
}
}
}

?>

  </tbody>
</table>
















<?php } 

if(($_GET['add'] == 1 && $_GET['step'] == 1) || ($_GET['edit'] != '' && $_GET['step'] == 1)) {
  
  
?>
<form method="get" class="ccm-dashboard-content-form" action="">
	
	<fieldset>
		<legend><?= $titletext ?> an Entry</legend>
		<div class="form-group">
			<label for="SITE" class="launch-tooltip control-label" data-placement="right" title="">User</label>
                        <input type="hidden" id="SITE" name="entryID" value="<?php if($edit){ echo $edit['ID']; } ?>" class="span4 form-control ccm-input-text">
                        <input type="hidden" id="SITE" name="step" value="2" class="span4 form-control ccm-input-text">
                        <?php if($_GET['edit'] != ''){ ?>
                        <input type="hidden" id="SITE" name="edit" value="1" class="span4 form-control ccm-input-text">  
                        <?php }else{ ?>
                        <input type="hidden" id="SITE" name="add" value="1" class="span4 form-control ccm-input-text">
                        <?php } ?>
                        <!-- enter select for users -->
                        <label>Search</label>
                        <input class="form-control" id="userFill" placeholder="Type to Search"/>
                        <select id="userFillSelect" required class="form-control" name="entryUser">
                            <option value="">-- Select a User --</option>
                        <?php
                         $userSearch = $db->query("SELECT * From Users");
                         while($user = $userSearch->fetch()){
                           
                           $ui = UserInfo::getByID($user['uID']);
                           $fName = trim(h($ui->getAttribute('first_name')));
                           $lName = trim(h($ui->getAttribute('last_name')));
                         ?>
                            <option value="<?= $user['uID']; ?>" <?php if($user['uID'] == $edit['userID']){ echo 'Selected'; } ?>><?= $user['uName'] . ' - ' . $fName . ' ' . $lName; ?></option> 
                        <?php    
                         }
                        ?>
                        </select>
                          <script>
                          $("#userFill").bind("keyup change", function(e) {
                              var optionsArray = [];
                              var thisValue = $(this).val();
                              var thisValueLength = thisValue.length;
                              var thisLowerCaseValue = thisValue.toLowerCase();
                              
                              $("#userFillSelect option").each(function()
                                {
                                    var optionVal = $(this).val();
                                    var optionText = $(this).text();
                                    var LCoptionText = optionText.toLowerCase();
                                    if (!LCoptionText.includes(thisLowerCaseValue)){
                                    // Add $(this).val() to your list
                                    optionsArray.push($(this).val());
                                    $(this).hide();
                                    }else{
                                      $(this).show();
                                    }
                                });
                            
                              $(optionsArray).each(function(){
                                
                              });
                              
                              console.log(thisValueLength);
                              
                              if(thisValueLength < 1){
                                $('#userFillSelect').attr("size", "");
                              }else{
                                $('#userFillSelect').attr("size", "10");
                              }
                              
                              
                          })
                        </script>
                </div>
                
                <div class="form-group">
			<label for="SITE" class="launch-tooltip control-label" data-placement="right" title="">Event</label>
                        <!-- enter select for events -->
                        <select class="form-control" name="entryEvent">
                        <?php
                         $eventSearch = $db->query("SELECT * From leaderboard_leaderboard_events");
                         while($event = $eventSearch->fetch()){
                           
                         ?>
                            <option value="<?= $event['ID']; ?>" <?php if($event['ID'] == $edit['eventID']){ echo 'Selected'; } ?>><?= $event['eventName']; ?></option> 
                        <?php    
                         }
                        ?>  
                        </select>
                </div>
                
                <div class="form-group">
			<label for="SITE" class="launch-tooltip control-label" data-placement="right" title="">Game</label>
                        <!-- enter select for games -->
                        <select class="form-control" name="entryGame">
                        <?php
                         $gameSearch = $db->query("SELECT * From leaderboard_games");
                         while($game = $gameSearch->fetch()){
                           
                         ?>
                            <option value="<?= $game['ID']; ?>" <?php if($game['ID'] == $edit['gameID']){ echo 'Selected'; } ?>><?= $game['gameTitle']; ?></option> 
                        <?php    
                         }
                        ?>  
                        </select>
                </div>
                
                <div class="form-group">
			<label for="SITE" class="launch-tooltip control-label" data-placement="right" title="">Paid?</label>
                        <!-- enter checkbox for paid -->
                        <br/><input type="checkbox" name="paid" value="1" <?php if($edit['paid'] == 1){ echo 'checked'; } ?>/>
                </div>
                
                <div class="form-group">
			<label for="SITE" class="launch-tooltip control-label" data-placement="right" title="">Attended?</label>
                        <!-- enter checkbox for attended -->
                        <br/><input type="checkbox" name="attended" value="1" <?php if($edit['attended'] == 1){ echo 'checked'; } ?>/>
                </div>
                
                <div class="form-group">
			<label for="SITE" class="launch-tooltip control-label" data-placement="right" title="">To Show on Event Leaderboard?</label>
                        <!-- enter checkbox for if shown on leaderboard -->
                        <br/><input type="checkbox" name="leaderboard" value="1" <?php if($edit['show_on_leaderboard'] == 1){ echo 'checked'; } ?>/>
                </div>
                
                <div class="form-group">
			<label for="SITE" class="launch-tooltip control-label" data-placement="right" title="">To Show on Overall Leaderboard for Game?</label>
                        <!-- enter checkbox for if shown on overall  game leaderboard -->
                        <br/><input type="checkbox" name="overallLeaderboard" value="1" <?php if($edit['show_on_total_leaderboard'] == 1){ echo 'checked'; } ?>/>
                </div>
	</fieldset>

	
	<div class="ccm-dashboard-form-actions-wrapper">
	<div class="ccm-dashboard-form-actions">
		<button class="pull-right btn btn-primary" type="submit">Next</button>
	</div>
	</div>
</form>
<?php
}else if($_GET['step'] == 2){
  
//add in the entry to DB and get ID back

if($_GET['add'] == 1 && $_GET['entryUser'] != ''){
  
  if($_GET['entryUser'] != ''){
    $entryUser = $_GET['entryUser'];
  }
  
  if($_GET['entryEvent'] != ''){
    $entryEvent = $_GET['entryEvent'];
  }
  
  if($_GET['entryGame'] != ''){
    $entryGame = $_GET['entryGame'];
  }
  
  if($_GET['paid'] != ''){
    $paid = $_GET['paid'];
  }
  
  if($_GET['attended'] != ''){
    $attended = $_GET['attended'];
  }
  
  if($_GET['leaderboard'] != ''){
    $leaderboard = $_GET['leaderboard'];
  }
  
  if($_GET['overallLeaderboard'] != ''){
    $overallLeaderboard = $_GET['overallLeaderboard'];
  }
  
  
  if($db->query("INSERT INTO `leaderboard_entries` (`userID`, `eventID`, `gameID`, `paid`, `attended`, `show_on_leaderboard`, `show_on_total_leaderboard`) VALUES (?, ?, ?, ?, ?, ?, ?)", array($entryUser, $entryEvent, $entryGame, $paid, $attended, $leaderboard, $overallLeaderboard))){
    $message = 'Leaderboard Entry successfully set up, add the score(s) below!';
    $success = 1;
    
    $lastID = $db->query('SELECT MAX(ID) as entryID FROM `leaderboard_entries`');
    $entry = $lastID->fetch();
    $entryID = $entry['entryID'];
    
  }else{
    $message = 'Sorry, there was an issue setting up your entry, please go back and try again';
  }
  
  
}  

if($_GET['edit'] != '' && $_GET['entryUser'] != ''){
  
  $id = $_GET['edit'];
  $entryID = $_GET['edit'];
  
  if($_GET['entryUser'] != ''){
    $entryUser = $_GET['entryUser'];
  }
  
  if($_GET['entryEvent'] != ''){
    $entryEvent = $_GET['entryEvent'];
  }
  
  if($_GET['entryGame'] != ''){
    $entryGame = $_GET['entryGame'];
  }
  
  if($_GET['paid'] != ''){
    $paid = $_GET['paid'];
  }
  
  if($_GET['attended'] != ''){
    $attended = $_GET['attended'];
  }
  
  if($_GET['leaderboard'] != ''){
    $leaderboard = $_GET['leaderboard'];
  }
  
  if($_GET['overallLeaderboard'] != ''){
    $overallLeaderboard = $_GET['overallLeaderboard'];
  }
  
  
  if($db->query("UPDATE `leaderboard_entries` SET `userID`=?, `eventID`=?, `gameID`=?, `paid`=?, `attended`=?, `show_on_leaderboard`=?, `show_on_total_leaderboard`=? WHERE `ID`=?", array($entryUser, $entryEvent, $entryGame, $paid, $attended, $leaderboard, $overallLeaderboard, $id))){
    $message = 'Leaderboard Entry successfully updated, edit the score(s) below!';
    $success = 1;
    
    $lastID = $db->query('SELECT MAX(ID) as entryID FROM `leaderboard_entries`');
    $entry = $lastID->fetch();
    $entryID = $entry['entryID'];
    
  }else{
    $message = 'Sorry, there was an issue setting up your entry, please go back and try again';
  }
  
  
}


if($message != ''){
  echo '<div class="alert alert-success">' . $message . '</div>';
}
  

if($entryID != ''){
?>
<form method="post" class="ccm-dashboard-content-form" action="<?php if($_GET['edit'] != ''){ echo $this->action('update'); }else if($_GET['add'] == 1){ echo $this->action('add'); } ?>">
	
	<fieldset>
		<legend>Add/Edit Score Values</legend>

<?php
//select score types and display field for each one associated to the game    
if($_GET['entryGame'] != ''){
$scoreSearch = $db->query("SELECT * From leaderboard_game_scoring LEFT JOIN leaderboard_game_score_types ON leaderboard_game_scoring.scoreTypeID = leaderboard_game_score_types.ID WHERE gameID = ?", array($_GET['entryGame']));
while($score = $scoreSearch->fetch()){
  
if($_GET['edit'] != ''){
  $valueSearch = $db->query("SELECT score FROM leaderboard_entry_scores WHERE ID = ?", array($_GET['edit']));
  $value = $valueSearch->fetch();
}  
 
?>
<div class="form-group">
			<label for="SITE" class="launch-tooltip control-label" data-placement="right" title=""><?= $score['typeName'] ?> Value</label>
                        <!-- enter checkbox for if shown on overall  game leaderboard -->
                        <br/><input type="number" class="form-control" name="<?= $score['scoreTypeID'] ?>" value="<?php if($value){ echo $value['score']; } ?>"/>
</div>
    
<?php    
  }
  
}
?>  
    
   <input type="hidden" name="entryID" value="<?= $entryID ?>"/> 
   <input type="hidden" name="gameID" value="<?= $entryGame ?>"/>
    
    
  </fieldset>
    
  <div class="ccm-dashboard-form-actions-wrapper">
	<div class="ccm-dashboard-form-actions">
		<button class="pull-right btn btn-primary" type="submit">Save Scores</button>
	</div>
	</div>
    
</form>
<?php }

} ?> 


