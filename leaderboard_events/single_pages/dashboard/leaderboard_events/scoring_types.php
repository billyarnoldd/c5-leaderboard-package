<?php
if($_GET['add'] == '1' || $_GET['edit'] != ''){
    
}else{
?>
<a href='?add=1' class='btn btn-primary'>Add a new Score Type</a>
<br/><br/>
<?php
}


if(is_array($scoreTypes) && $_GET['add'] != '1' && $_GET['edit'] == ''){
?>    
<table border="0" cellspacing="0" cellpadding="0" class="ccm-search-results-table">
	<thead>
            <tr>
                <th class="">Type Name</th>
                <th class=''>Icon</th>
                <th class="">Actions</th>
            </tr>
        </thead>
	<tbody>
            
    
<?php    
foreach($scoreTypes as $type){
    $f = \File::getByID($type['typeIcon']);
    
    echo "<td>" . $type['typeName'];
    echo "</td>";
    echo "<td>" . $f->getURL();
    echo "</td>";
    echo "<td>" . "<a href='?edit=".$type['ID'] ."' class='btn btn-primary'>Edit</a>";
    echo "<a style='margin-left:15px;' href='". $this->action('delete', $type['ID']) ."' class='btn btn-danger'>Delete</a>";
    echo "</td>";
    echo "</tr>";
}


?>
    </tbody>
</table>
<?php } ?>


<?php if($_GET['add'] != '' || $_GET['edit'] != ''){ 
    
$db = \Database::connection();

if($_GET['edit'] != ''){
$editsearch = $db->query("SELECT * FROM `leaderboard_game_score_types` WHERE ID = ?", array($_GET['edit']));
$edit = $editsearch->fetch();

$titletext = 'Edit';
}else{
$titletext = 'Add';    
}    
    
?>

<form method="post" enctype="multipart/form-data" class="ccm-dashboard-content-form" action="<?php if($_GET['edit'] != ''){ echo $this->action('update'); }else{ echo $this->action('add'); } ?>">
	
	<fieldset>
		<legend><?= $titletext ?> a Score Type</legend>
		<div class="form-group">
			<label for="SITE" class="launch-tooltip control-label" data-placement="right" title="">Name of Score Type</label>
                        <input type="text" id="SITE" name="typeName" value="<?php if($edit){ echo $edit['typeName']; } ?>" class="span4 form-control ccm-input-text">
                        
                        <input type="hidden" id="SITE" name="typeID" value="<?php if($edit){ echo $edit['ID']; } ?>" class="span4 form-control ccm-input-text">	
                </div>
    
    <div class="form-group">
			<label for="SITE" class="launch-tooltip control-label" data-placement="right" title="">Leaderboard Priority (Lowest Number taken into account first)</label>
                        <input required type="number" name="leaderboardPriority" value="<?php if($edit){ echo $edit['leaderboardPriority']; } ?>" class="span4 form-control ccm-input-text">
                        
                </div>
    
    <div class="form-group">
			<label for="SITE" class="launch-tooltip control-label" data-placement="right" title="">Order of Scoring</label>
      <select class='form-control  ccm-input-select' name='scoreOrder' required>
          <option value='DESC'>Highest Score First</option>
          <option value='ASC'>Lowest Score First</option>
      </select>
                        
                </div>
                <div class="form-group">
                    <label class="control-label">Icon</label>
                    <?php
                        $service = Core::make('helper/concrete/file_manager');
                        print $service->file('image', 'image', $edit['typeIcon']);
                    ?>
                </div>
	</fieldset>

	
	<div class="ccm-dashboard-form-actions-wrapper">
	<div class="ccm-dashboard-form-actions">
            <a href='?' class='pull-left btn btn-primary'>Back</a>
		<button class="pull-right btn btn-primary" type="submit">Add</button>
	</div>
	</div>
</form>


<?php }