<?php
if(is_array($events) && $_GET['add'] != '1' && $_GET['edit'] == ''){
?>    
<a href='?add=1' class='btn btn-primary'>Add a new Event</a>
<br/><br/>
<table border="0" cellspacing="0" cellpadding="0" class="ccm-search-results-table">
	<thead>
            <tr>
                <th class="">Event Name</th>
                <th class="">Actions</th>
            </tr>
        </thead>
	<tbody>
            
    
<?php    
foreach($events as $event){
    echo "<td>" . $event['eventName'];
    echo "</td>";
    echo "<td>" . "<a href='?edit=".$event['ID'] ."' class='btn btn-primary'>Edit</a>";
    echo "<a style='margin-left:15px;' href='". $this->action('delete', $event['ID']) ."' class='btn btn-danger'>Delete</a>";
    echo "</td>";
    echo "</tr>";
}


?>
    </tbody>
</table>
<?php } ?>


<?php if($_GET['add'] != '' || $_GET['edit'] != ''){ 
    
$db = \Database::connection();

if($_GET['edit'] != ''){
$editsearch = $db->query("SELECT * FROM `leaderboard_leaderboard_events` WHERE ID = ?", array($_GET['edit']));
$edit = $editsearch->fetch();

$titletext = 'Edit';
}else{
$titletext = 'Add';    
}    
    
?>

<form method="post" class="ccm-dashboard-content-form" action="<?php if($_GET['edit'] != ''){ echo $this->action('update'); }else{ echo $this->action('add'); } ?>">
	
	<fieldset>
		<legend><?= $titletext ?> an Event</legend>
		<div class="form-group">
			<label for="SITE" class="launch-tooltip control-label" data-placement="right" title="" data-original-title="This will be the name of the new event created.">Event Name</label>
                        <input type="text" id="SITE" name="eventName" value="<?php if($edit){ echo $edit['eventName']; } ?>" class="span4 form-control ccm-input-text">
                        <input type="hidden" id="SITE" name="eventID" value="<?php if($edit){ echo $edit['ID']; } ?>" class="span4 form-control ccm-input-text">	
                </div>
                <div class='form-group'>
                    <label for="SITE" class="launch-tooltip control-label" data-placement="right" title="" >Event Games</label> 
                    <br/>
                        <?php
                    $gameListSearch = $db->query("SELECT * FROM `leaderboard_games`");
                    while($gameList = $gameListSearch->fetch()){
                        
                        $assocsearch = $db->query("SELECT * FROM `leaderboard_event_games` WHERE `eventID` = ? AND `gameID` = ?", array($edit['ID'],$gameList['ID']));
                        $assoc = $assocsearch->fetch();
                    ?>
                        <label><?= $gameList['gameTitle'] ?></label>
                        <input type='checkbox' name='eventGames[]' value='<?= $gameList['ID'] ?>' <?php if(!empty($assoc)){ echo 'checked';} ?>/><br/>
                    <?php
                    }
                    
                    ?>
                </div>
	</fieldset>

	
	<div class="ccm-dashboard-form-actions-wrapper">
	<div class="ccm-dashboard-form-actions">
		<button class="pull-right btn btn-primary" type="submit">Add</button>
	</div>
	</div>
</form>

<?php }