<?php
if($_GET['add'] == '1' || $_GET['edit'] != ''){
    
}else{
?>
<a href='?add=1' class='btn btn-primary'>Add a new Game</a>
<br/><br/>
<?php
}


if(is_array($games) && $_GET['add'] != '1' && $_GET['edit'] == ''){
?>    
<table border="0" cellspacing="0" cellpadding="0" class="ccm-search-results-table">
	<thead>
            <tr>
                <th class="">Game Title</th>
                <th class=''>Age Rating</th>
                <th class=''>Image</th>
                <th class="">Actions</th>
            </tr>
        </thead>
	<tbody>
            
    
<?php    
foreach($games as $game){
    $f = \File::getByID($game['gameImage']);
    if(!empty($f)){ $url = $f->getURL(); }
    
    echo "<td>" . $game['gameTitle'];
    echo "</td>";
    echo "<td>" . $game['gameAgeRating'];
    echo "</td>";
    echo "<td>" . $url;
    echo "</td>";
    echo "<td>" . "<a href='?edit=".$game['ID'] ."' class='btn btn-primary'>Edit</a>";
    echo "<a style='margin-left:15px;' href='". $this->action('delete', $game['ID']) ."' class='btn btn-danger'>Delete</a>";
    echo "</td>";
    echo "</tr>";
}


?>
    </tbody>
</table>
<?php } ?>


<?php if($_GET['add'] != '' || $_GET['edit'] != ''){ 
    
$db = \Database::connection();

if($_GET['edit'] != ''){
$editsearch = $db->query("SELECT * FROM `leaderboard_games` WHERE ID = ?", array($_GET['edit']));
$edit = $editsearch->fetch();

$titletext = 'Edit';
}else{
$titletext = 'Add';    
}    
    
?>

<form method="post" enctype="multipart/form-data" class="ccm-dashboard-content-form" action="<?php if($_GET['edit'] != ''){ echo $this->action('update'); }else{ echo $this->action('add'); } ?>">
	
	<fieldset>
		<legend><?= $titletext ?> a Game</legend>
		<div class="form-group">
			<label for="SITE" class="launch-tooltip control-label" data-placement="right" title="" data-original-title="This will be the name of the new game created.">Game Title</label>
                        <input type="text" id="SITE" name="gameName" value="<?php if($edit){ echo $edit['gameTitle']; } ?>" class="span4 form-control ccm-input-text">
                        
                        <input type="hidden" id="SITE" name="gameID" value="<?php if($edit){ echo $edit['ID']; } ?>" class="span4 form-control ccm-input-text">	
                </div>
                <div class='form-group'>
                        <label for="SITE" class="launch-tooltip control-label" data-placement="right" title="" data-original-title="The age rating of the game">Age Rating</label>
                        <input type="number" id="SITE" name="ageRating" value="<?php if($edit){ echo $edit['gameAgeRating']; } ?>" class="span4 form-control ccm-input-text">
                </div>
                <div class="form-group">
                    <label class="control-label">Image</label>
                    <?php
                        $service = Core::make('helper/concrete/file_manager');
                        print $service->file('image', 'image', $edit['gameImage']);
                    ?>
                </div>
                <div class='form-group'>
                    <label for="SITE" class="launch-tooltip control-label" data-placement="right" title="" >Games Score Types</label> 
                    <br/>
                        <?php
                    $gameListSearch = $db->query("SELECT * FROM `leaderboard_game_score_types`");
                    while($gameList = $gameListSearch->fetch()){
                        
                        $assocsearch = $db->query("SELECT * FROM `leaderboard_game_scoring` WHERE `gameID` = ? AND `scoreTypeID` = ?", array($edit['ID'],$gameList['ID']));
                        $assoc = $assocsearch->fetch();
                    ?>
                        <label><?= $gameList['typeName'] ?></label>
                        <input type='checkbox' name='eventGames[]' value='<?= $gameList['ID'] ?>' <?php if(!empty($assoc)){ echo 'checked';} ?>/><br/>
                    <?php
                    }
                    
                    ?>
                </div>
	</fieldset>

	
	<div class="ccm-dashboard-form-actions-wrapper">
	<div class="ccm-dashboard-form-actions">
            <a href='?' class='pull-left btn btn-primary'>Back</a>
		<button class="pull-right btn btn-primary" type="submit">Add</button>
	</div>
	</div>
</form>


<?php }
