<?php
namespace Concrete\Package\LeaderboardEvents\Controller\SinglePage\Dashboard\LeaderboardEvents;
use \Concrete\Core\Page\Controller\DashboardPageController;

defined('C5_EXECUTE') or die("Access Denied.");

class LeaderboardEvents extends DashboardPageController
{
    public $eventName;
    
    public function view()
    {
        $db = \Database::connection();
        
        //how to select the leaderboard\\
        //SELECT leaderboard_entries.ID, a.scoreTypeID as ScoreTypeA, a.score as ScoreA, b.scoreTypeID as ScoreTypeb, b.score as ScoreB FROM leaderboard_entries 
        //LEFT JOIN `leaderboard_entries` as a ON `leaderboard_entries`.`ID` = `a`.`eventID` 
        //AND (a.scoreTypeID = 2) 
        //LEFT JOIN `leaderboard_entries` as b ON `leaderboard_entries`.`ID` = `b`.`eventID` 
        //AND (b.scoreTypeID = 1)

        //to generate the above::

        //user something to loop through score type of a game and if theres 1 only generate A, if 2 generate B as Well etc..
        //remember to generate scoretype order a, b, c etc...
        
        $leaderboardSearch = $db->query("
        SELECT *, a.scoreTypeID as ScoreTypeA, a.score as ScoreA, b.scoreTypeID as ScoreTypeb, b.score as ScoreB,  c.scoreTypeID as ScoreTypec, c.score as ScoreC FROM leaderboard_entries 
        LEFT JOIN `leaderboard_entry_scores` as c ON `leaderboard_entries`.`ID` = `c`.`entryID` 
        AND (c.scoreTypeID = 3)
        LEFT JOIN `leaderboard_entry_scores` as a ON `leaderboard_entries`.`ID` = `a`.`entryID` 
        AND (a.scoreTypeID = 1) 
        LEFT JOIN `leaderboard_entry_scores` as b ON `leaderboard_entries`.`ID` = `b`.`entryID` 
        AND (b.scoreTypeID = 2)"  
          );
        $leaderboard = $leaderboardSearch->fetchAll();
        $this->set('leaderboard', $leaderboard);
    }


}

