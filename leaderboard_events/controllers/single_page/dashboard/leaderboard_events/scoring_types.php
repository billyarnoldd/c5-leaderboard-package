<?php
namespace Concrete\Package\LeaderboardEvents\Controller\SinglePage\Dashboard\LeaderboardEvents;
use \Concrete\Core\Page\Controller\DashboardPageController;

defined('C5_EXECUTE') or die("Access Denied.");

class ScoringTypes extends DashboardPageController
{
    public $eventName;
    
    public function view()
    {
        $db = \Database::connection();
        
        $eventListSearch = $db->query("SELECT * FROM `leaderboard_game_score_types`");
        $eventList = $eventListSearch->fetchAll();
        $this->set('scoreTypes', $eventList);
    }
    
    public function add()
    {
        $db = \Database::connection();
        
        if(isset($_POST['typeName'])){
            $this->typeName = $_POST['typeName'];
            $this->image = $_POST['image'];
            $this->leaderboardPriority = $_POST['leaderboardPriority'];
            $this->scoreOrder = $_POST['scoreOrder'];
        
            if($db->query("INSERT INTO `leaderboard_game_score_types` (`typeName`, `typeIcon`, `leaderboardPriority`, `scoreORder`) VALUES (?, ?, ?, ?)", array($this->typeName, $this->image, $this->leaderboardPriority, $this->scoreOrder))){
                
                $this->set('success', 'Score Type Added Succesfully');
                $this->view();
            }else{
                $this->error->add('There was a problem, try again.');
                $this->view();
            }
        
        }else{
            return false;
        }
    }
    
    public function update()
    {
         $db = \Database::connection();
        
        if(isset($_POST['typeName'])){
            $this->typeName = $_POST['typeName'];
            $this->image = $_POST['image'];
            $this->typeID = $_POST['typeID'];
            $this->leaderboardPriority = $_POST['leaderboardPriority'];
            $this->scoreOrder = $_POST['scoreOrder'];
        
            if($db->query("UPDATE `leaderboard_game_score_types` SET `typeName`= ?, `typeIcon` = ?, `leaderboardPriority` = ?, `scoreOrder` = ? WHERE `ID` = ?", array($this->typeName, $this->image, $this->leaderboardPriority, $this->scoreOrder, $this->typeID))){
                
                 $this->set('success', 'Score Type Updated Succesfully');
                $this->view();
            }else{
                $this->error->add('There was a problem, try again.');
                $this->view();
            }
        
        }else{
            return false;
        }
    }
    
    public function delete($eventID)
    {
         $db = \Database::connection();
         
            if($db->query("DELETE FROM `leaderboard_game_score_types` WHERE ID = ?", array($eventID))){
                $this->set('success', 'Score Type Deleted Succesfully');
                $this->view();
            }else{
                $this->error->add('There was a problem, try again.');
                $this->view();
            }
    }
}
   
