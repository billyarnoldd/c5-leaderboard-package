<?php
namespace Concrete\Package\LeaderboardEvents\Controller\SinglePage\Dashboard\LeaderboardEvents;
use \Concrete\Core\Page\Controller\DashboardPageController;

defined('C5_EXECUTE') or die("Access Denied.");

class LeaderboardEntries extends DashboardPageController
{
    public $eventName;
    
    public function view()
    {
        $db = \Database::connection();
        
        $leaderboardSearch = $db->query("
        SELECT *, leaderboard_entries.ID as ENTRYID, a.scoreTypeID as ScoreTypeA, a.score as ScoreA, b.scoreTypeID as ScoreTypeb, b.score as ScoreB,  c.scoreTypeID as ScoreTypec, c.score as ScoreC FROM leaderboard_entries 
        LEFT JOIN `leaderboard_entry_scores` as c ON `leaderboard_entries`.`ID` = `c`.`entryID` 
        AND (c.scoreTypeID = 3)
        LEFT JOIN `leaderboard_entry_scores` as a ON `leaderboard_entries`.`ID` = `a`.`entryID` 
        AND (a.scoreTypeID = 1) 
        LEFT JOIN `leaderboard_entry_scores` as b ON `leaderboard_entries`.`ID` = `b`.`entryID` 
        AND (b.scoreTypeID = 2)"  
          );
        $leaderboard = $leaderboardSearch->fetchAll();
        $this->set('leaderboard', $leaderboard);
    }
    
    public function add()
    {
        $db = \Database::connection();
        
        if(isset($_POST['entryID'])){
            
            $this->entryID = $_POST['entryID'];
            $this->gameID = $_POST['gameID'];
            
            $scoreDelete = $db->query("DELETE FROM `leaderboard_entry_scores` WHERE `entryID` = ?", array($this->entryID));
            
            
            //Search for score types and insert values into the database for the entry
            $scoreSearch = $db->query("SELECT * From leaderboard_game_scoring LEFT JOIN leaderboard_game_score_types ON leaderboard_game_scoring.scoreTypeID = leaderboard_game_score_types.ID WHERE gameID = ?", array($this->gameID));
            while($score = $scoreSearch->fetch()){
              //for each score type id, insert the entryID, ScoreTypeID and Value into the database
              
              if($_POST[$score['scoreTypeID']] != ''){
              $totalScore = $_POST[$score['scoreTypeID']];
              }else{
                $totalScore = 0;
              }
              
              if($db->query("INSERT INTO `leaderboard_entry_scores` (`entryID`, `scoreTypeID`, `score`) VALUES (?, ?, ?)", array($this->entryID, $score['scoreTypeID'], $totalScore))){
                
                $this->set('success', 'Scores Added Succesfully');
                $this->view();
            }else{
                $this->error->add('There was a problem, try again.');
                $this->view();
            }
              
              
              
            }
        
           
        }else{
            return false;
        }
    }
    
    public function update()
    {
      $db = \Database::connection();
        
        if(isset($_POST['entryID'])){
            
            $this->entryID = $_POST['entryID'];
            $this->gameID = $_POST['gameID'];
            
            $scoreDelete = $db->query("DELETE FROM `leaderboard_entry_scores` WHERE `entryID` = ?", array($this->entryID));
            
            
            //Search for score types and insert values into the database for the entry
            $scoreSearch = $db->query("SELECT * From leaderboard_game_scoring LEFT JOIN leaderboard_game_score_types ON leaderboard_game_scoring.scoreTypeID = leaderboard_game_score_types.ID WHERE gameID = ?", array($this->gameID));
            while($score = $scoreSearch->fetch()){
              //for each score type id, insert the entryID, ScoreTypeID and Value into the database
              
              if($_POST[$score['scoreTypeID']] != ''){
              $totalScore = $_POST[$score['scoreTypeID']];
              }else{
                $totalScore = 0;
              }
              
              if($db->query("INSERT INTO `leaderboard_entry_scores` (`entryID`, `scoreTypeID`, `score`) VALUES (?, ?, ?)", array($this->entryID, $score['scoreTypeID'], $totalScore))){
                
                $this->set('success', 'Scores Added Succesfully');
                $this->view();
            }else{
                $this->error->add('There was a problem, try again.');
                $this->view();
            }
              
              
              
            }
        
           
        }else{
            return false;
        }
    }
    
    public function delete($eventID)
    {
         $db = \Database::connection();
         
            if($db->query("DELETE FROM `leaderboard_entries` WHERE ID = ?", array($eventID))){
                $this->set('success', 'Score Entry Deleted Succesfully');
                $this->view();
            }else{
                $this->error->add('There was a problem, try again.');
                $this->view();
            }
    }
}
