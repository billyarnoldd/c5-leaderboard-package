<?php
namespace Concrete\Package\LeaderboardEvents\Controller\SinglePage\Dashboard\LeaderboardEvents;
use \Concrete\Core\Page\Controller\DashboardPageController;

defined('C5_EXECUTE') or die("Access Denied.");

class Events extends DashboardPageController
{
    public $eventName;
    
    public function view()
    {
        $db = \Database::connection();
        
        $eventListSearch = $db->query("SELECT * FROM `leaderboard_leaderboard_events`");
        $eventList = $eventListSearch->fetchAll();
        $this->set('events', $eventList);
    }
    
    public function add()
    {
        $db = \Database::connection();
        
        if(isset($_POST['eventName'])){
            $this->eventName = $_POST['eventName'];
            $this->eventGames = $_POST['eventGames'];
        
            if($db->query("INSERT INTO `leaderboard_leaderboard_events` (`eventName`) VALUES (?)", array($this->eventName))){
                
                $eventIDsearch = $db->query("SELECT MAX(ID) as NewID FROM `leaderboard_games`");
                $eventID = $eventIDsearch->fetch();
                
                if(is_array($this->eventGames)){
                    foreach($this->eventGames as $eventGame){
                        $db->query("INSERT INTO `leaderboard_event_games` (`eventID`, `gameID`) VALUES (?, ?)", array($eventID['NewID'], $eventGame));
                    }
                }
                
                $this->set('success', 'Leaderboard Added Succesfully');
                $this->view();
            }else{
                $this->error->add('There was a problem, try again.');
                $this->view();
            }
        
        }else{
            return false;
        }
    }
    
    public function update()
    {
         $db = \Database::connection();
        
        if(isset($_POST['eventName'])){
            $this->eventName = $_POST['eventName'];
            $this->eventID = $_POST['eventID'];
            
            $this->eventGames = $_POST['eventGames'];
        
            if($db->query("UPDATE `leaderboard_leaderboard_events` SET `eventName`= ? WHERE `ID` = ?", array($this->eventName, $this->eventID))){
                
                
                $db->query("DELETE FROM `leaderboard_event_games` WHERE `eventID` = ?", array($this->eventID));
                
                if(is_array($this->eventGames)){
                    foreach($this->eventGames as $eventGame){
                        $db->query("INSERT INTO `leaderboard_event_games` (`eventID`, `gameID`) VALUES (?, ?)", array($this->eventID, $eventGame));
                    }
                }
                
                
                 $this->set('success', 'Leaderboard Updated Succesfully');
                $this->view();
            }else{
                $this->error->add('There was a problem, try again.');
                $this->view();
            }
        
        }else{
            return false;
        }
    }
    
    public function delete($eventID)
    {
         $db = \Database::connection();
         
            if($db->query("DELETE FROM `leaderboard_leaderboard_events` WHERE ID = ?", array($eventID))){
                $this->set('success', 'Leaderboard Event Updated Succesfully');
                $this->view();
            }else{
                $this->error->add('There was a problem, try again.');
                $this->view();
            }
    }
    
    
}