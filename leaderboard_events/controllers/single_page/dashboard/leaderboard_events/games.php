<?php
namespace Concrete\Package\LeaderboardEvents\Controller\SinglePage\Dashboard\LeaderboardEvents;
use \Concrete\Core\Page\Controller\DashboardPageController;

defined('C5_EXECUTE') or die("Access Denied.");

class Games extends DashboardPageController
{
    public $eventName;
    
    public function view()
    {
        $db = \Database::connection();
        
        $eventListSearch = $db->query("SELECT * FROM `leaderboard_games`");
        $eventList = $eventListSearch->fetchAll();
        $this->set('games', $eventList);
        $this->set('test', 'test');
    }
    
    public function add()
    {
        $db = \Database::connection();
        
        if(isset($_POST['gameName'])){
            $this->gameName = $_POST['gameName'];
            $this->ageRating = $_POST['ageRating'];
            $this->image = $_POST['image'];
            $this->eventGames = $_POST['eventGames'];
        
            if($db->query("INSERT INTO `leaderboard_games` (`gameTitle`, `gameAgeRating`, `gameImage`) VALUES (?, ?, ?)", array($this->gameName, $this->ageRating, $this->image))){
                
                $gameIDsearch = $db->query("SELECT MAX(ID) as NewID FROM `leaderboard_games`");
                $gameID = $gameIDsearch->fetch();
                
                if(is_array($this->eventGames)){
                    foreach($this->eventGames as $eventGame){
                        $db->query("INSERT INTO `leaderboard_game_scoring` (`gameID`, `scoreTypeID`) VALUES (?, ?)", array($gameID['NewID'], $eventGame));
                    }
                }
                
                $this->set('success', 'Game Added Succesfully');
                $this->view();
            }else{
                $this->error->add('There was a problem, try again.');
                $this->view();
            }
        
        }else{
            return false;
        }
    }
    
    public function update()
    {
         $db = \Database::connection();
        
        if(isset($_POST['gameName'])){
            $this->gameName = $_POST['gameName'];
            $this->ageRating = $_POST['ageRating'];
            $this->image = $_POST['image'];
            $this->gameID = $_POST['gameID'];
            $this->eventGames = $_POST['eventGames'];
        
            if($db->query("UPDATE `leaderboard_games` SET `gameTitle`= ?, `gameAgeRating` = ?, `gameImage` = ? WHERE `ID` = ?", array($this->gameName, $this->ageRating, $this->image, $this->gameID))){
                
                $db->query("DELETE FROM `leaderboard_game_scoring` WHERE `gameID` = ?", array($this->gameID));
                
                if(is_array($this->eventGames)){
                    foreach($this->eventGames as $eventGame){
                        $db->query("INSERT INTO `leaderboard_game_scoring` (`gameID`, `scoreTypeID`) VALUES (?, ?)", array($this->gameID, $eventGame));
                    }
                }
                
                
                $this->set('success', 'Game Updated Succesfully');
                $this->view();
            }else{
                $this->error->add('There was a problem, try again.');
                $this->view();
            }
        
        }else{
            return false;
        }
    }
    
    public function delete($eventID)
    {
         $db = \Database::connection();
         
            if($db->query("DELETE FROM `leaderboard_games` WHERE ID = ?", array($eventID))){
                $this->set('success', 'Game Deleted Succesfully');
                $this->view();
            }else{
                $this->error->add('There was a problem, try again.');
                $this->view();
            }
    }
    
   
}