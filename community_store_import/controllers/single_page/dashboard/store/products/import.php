<?php

namespace Concrete\Package\CommunityStoreImport\Controller\SinglePage\Dashboard\Store\Products;



use Concrete\Core\Controller\Controller;
use Concrete\Core\Page\Controller\DashboardPageController;
use Concrete\Core\Package\Package as Package;
use Concrete\Core\File\File;
use Log;
use Exception;
use Config;
use Core;
use Page;

use Concrete\Package\CommunityStore\Src\CommunityStore\Product\ProductList;
use Concrete\Package\CommunityStore\Src\CommunityStore\Product\Product;
use Concrete\Package\CommunityStore\Src\Attribute\Key\StoreProductKey as StoreProductKey;
use Concrete\Package\CommunityStore\Src\CommunityStore\Product\ProductLocation as StoreProductLocation;
use Concrete\Package\CommunityStore\Src\CommunityStore\Product\ProductImage as StoreProductImage;
use Concrete\Package\CommunityStore\Src\CommunityStore\Group\Group as StoreGroup;
use Concrete\Package\CommunityStore\Src\CommunityStore\Product\ProductGroup;
use Concrete\Core\Attribute\Key\Category;

use Concrete\Package\CommunityStore\Src\CommunityStore\Product\ProductOption\ProductOption;
use Concrete\Package\CommunityStore\Src\CommunityStore\Product\ProductOption\ProductOptionItem;

use Concrete\Package\CommunityStore\Src\CommunityStore\Product\ProductVariation\ProductVariation;
use Concrete\Package\CommunityStore\Src\CommunityStore\Product\ProductVariation\ProductVariationOptionItem;
use Concrete\Package\CommunityStore\Src\CommunityStore\Product\ProductOption\ProductOption as StoreProductOption;
use Concrete\Package\CommunityStore\Src\CommunityStore\Product\ProductVariation\ProductVariation as StoreProductVariation;


class Import extends DashboardPageController
{
    public $helpers = array('form', 'concrete/asset_library', 'json');
    private $attributes = array();

    public function view()
    {
        $this->loadFormAssets();
        $this->set('pageTitle', t('Product Import'));
    }

    public function loadFormAssets()
    {
        $this->requireAsset('core/file-manager');
        $this->requireAsset('core/sitemap');
        $this->requireAsset('css', 'select2');
        $this->requireAsset('javascript', 'select2');
        $this->set('concrete_asset_library', Core::make('helper/concrete/asset_library'));
        $this->set('form', Core::make('helper/form'));
    }
    
    public function testImport (){
      
      $db = \Database::connection();
      
      $productOptions['options'] = "Shoe_Size, Lacing";
      $productOptions['Shoe_Size'] = "Size_1, Size_2";
      $productOptions['Lacing'] = "Laces, Velcro";
      
      $productOptions['Size_1_sku'] = 'Item1SKU';
      $productOptions['Size_2_sku'] = 'Item2SKU';
      $productOptions['Lacing_sku'] = 'Item3SKU';
      $productOptions['Velcro_sku'] = 'Item4SKU';
      
      $productOptions['Size_1_price'] = 20;
      $productOptions['Size_2_price'] = 40;
      $productOptions['Lacing_price'] = 60;
      $productOptions['Velcro_price'] = 80;
      
      
      $data = array(
            'pSKU' => 'TEST SHOE',
            'pName' => 'TEST',
            'pDesc' => 'TEST Desc',
            'pDetail' => 'TEST',
            'pCustomerPrice' => '20',
            'pFeatured' => '0',
            'pQty' => '4',
            'pNoQty' => '1',
            'pTaxable' => '',
            'pActive' => '1',
            'pShippable' => '1',
            'pCreateUserAccount' => '',
            'pAutoCheckout' => '',
            'pExclusive' => '',

            'pPrice' => '40',
            'pSalePrice' => '35',
            'pPriceMaximum' => '',
            'pPriceMinimum' => '',
            'pPriceSuggestions' => '',
            'pQtyUnlim' => '1',
            'pBackOrder' => '',
            'pLength' => '',
            'pWidth' => '',
            'pHeight' => '',
            'pWeight' => '',
            'pNumberItems' => '',

            // CS v1.4.2+
            'pMaxQty' => '',
            'pQtyLabel' => '',
            'pAllowDecimalQty' => '',
            'pQtySteps' => '',
            'pSeperateShip' => '',
            'pPackageData' => '',

            // CS v2+
            'pQtyLabel' => (isset($row['pqtylabel']) ? $row['pqtylabel'] : ''),
            'pMaxQty' => (isset($row['pmaxqty']) ? $row['pmaxqty'] : 0),
            // Not supported in CSV data
            //'cID' => $row['cID'],
            'pfID' => '12',
           // 'pifID' => '12',
            'pVariations' => true,
            'pQuantityPrice' => true,
            'pTaxClass' => 1 ,
            'poName' => 'TEST',
            // 1 = default tax class
        );

        // Save product
        $p = Product::saveProduct($data);
        $productId = $p->getID();
        
        
        //Get product options in array
        $productOptionsArray = explode(", ", $productOptions['options']);
        
        $sortorder = 0;
        
        foreach($productOptionsArray as $productOption){
          
          //add product option
          
          $optionName = str_replace("_", " ", $productOption);
          
          
          $thisOption = ProductOption::add($p, $optionName, $sortorder, 'select', $productOption, false, true, 'select');
	  
          
          
          $OptionItemListArray = explode(", ", $productOptions[$productOption]);
          
          $itemSortOrder = 0;
          
          foreach($OptionItemListArray as $optionItem){
            
            $optionItemName = str_replace("_", " ", $optionItem);
            
            $thisOptionItem = ProductOptionItem::add($thisOption, $optionItemName, $itemSortOrder);
            
            //manually add variations
              //add Variations to Product and get ID
            
              $optionitemprice = $optionItem  .'_price';
              $optionitemprice2 = $productOptions[$optionitemprice];
              
              $optionitemsku = $optionItem  .'_sku';
              $optionitemsku2 = $productOptions[$optionitemsku];
              
              
            
              $data = [];
              $data['pvSKU'] = $optionitemsku2;
              $data['pvPrice'] = $optionitemprice2;
              $data['pvQtyUnlim'] = true;
              $data['pvSort'] = $itemSortOrder;
            
        //      $thisVariation = ProductVariation::add($productId, $data);


              //add variation to option item (needs to get first lot of combinations)
              
          //  $optionVariationLink = new ProductVariationOptionItem();  
          //  $optionVariationLink->setOption($thisOptionItem);
          //  $optionVariationLink->setVariation($thisVariation);
          //  $optionVariationLink->save();
            
            
            $itemSortOrder++;
          }
          
          
          
          $sortorder++;
        }
        
	
	$options = $p->getOptions();
	
	$optionsSearch = $db->query("SELECT poID FROM `CommunityStoreProductOptions` WHERE `pID` = ?", array($productId));
	$optionsArray = $optionsSearch->fetchAll();

        $optionArrays = [];
	
	//get the option ids and the option item ids

        if (!empty($optionsArray)) {
	    
            foreach ($optionsArray as $option) {
		
		$optionItemsSearch = $db->query("SELECT * FROM `CommunityStoreProductOptionItems` WHERE `poID` = ?", array($option['poID']));
		$optionItemsArray = $optionItemsSearch->fetchAll();
		
                $optID = $option['poID'];
		    
                    foreach ($optionItemsArray as $optItem) {
			$optItemID = $optItem['poiID'];
			
                        $optionArrays[$optID][] = $optItemID;
			                   
		    }
                
            }
        }else{
	    
	}

        $comboOptions = ProductVariation::combinations(array_values($optionArrays));
        
	
	
	$checkedOptions = [];

        foreach ($comboOptions as $option) {
            if (!is_array($option)) {
                $checkedOptions[] = [$option];
            } else {
                $checkedOptions[] = $option;
            }
        }

        $comboOptions = $checkedOptions;
        
	//loop through combinations
	
	//find the name of the poi from the id
	
	//get the values from the table based on the name to add to the value
	
	
	$p->save();
     //   $Variations = $p->getVariations();
        
       
      //  foreach($Variations as $variation){
      //    $variationPrice = 0;
          
         // $db->query("UPDATE `CommunityStoreProductVariations` SET `pvPrice` = ? WHERE `pID` = ?", array(50.00, $productId));
          
          
          
//          
//          $db = \Database::connection();
//          
//          
//          $handleSearch = $db->query("SELECT * FROM `CommunityStoreProductVariationOptionItems` LEFT JOIN `CommunityStoreProductOptionItems` ON `CommunityStoreProductVariationOptionItems`.`poiID` =  `CommunityStoreProductOptionItems`.`poiID` WHERE `pvID` = ?", array($variID));
//          
//          while($handles = $handleSearch->fetch()){
//            $itemHandle =  str_replace(" ", "_", $handles['poiName']);
//            echo $itemHandle . ' name<br/>';
//            $optionPriceHandle = $itemHandle . '_price';
//            $optionPrice = $productOptions[$optionPriceHandle];
//            $variationPrice = $variationPrice + $optionPrice;
//          }
//          
          //foreach($Options as $option){
            //$thisID = $option->getID();
            
            //echo $thisID . ' testing <br/>';
            
            //$prodOptItem = ProductOptionItem::getByID($option);
            
           // $optItemName = $prodOptItem->getName();
           // $optItemHandle = str_replace(" ", "_", $optItemName);
           // echo $optItemHandle .'name <br/>';
            
            //$optionID = $prodOptItem->getOptionID();
            
            //$finalOptionObject = ProductOption::getByID($optionID);
            
            //$newOption = $option->getOption();
            
            //echo $finalOptionObject->getHandle() . 'testing <br/>';
          //  $optionHandle = $finalOptionObject->getHandle();
          //  $optionPriceHandle = $optionHandle . '_price';
           // $optionPrice = $productOptions[$optionPriceHandle];
           // $variationPrice = $variationPrice + $optionPrice;
            
          //}
          
     //     $productBasePrice = $p->getBasePrice();
          
    //      $variationPrice = number_format((float)$variationPrice, 2) + number_format($productBasePrice, 2);
     //     $variation->setVariationPrice('50');
     //     $variation->save();
   //     }
        
        
        
        
    }

    public function run()
    {
        $this->saveSettings();

        $MAX_TIME = Config::get('community_store_import.max_execution_time');
        $MAX_EXECUTION_TIME = ini_get('max_execution_time');
        $MAX_INPUT_TIME = ini_get('max_input_time');
        ini_set('max_execution_time', $MAX_TIME);
        ini_set('max_input_time', $MAX_TIME);
        ini_set('auto_detect_line_endings', TRUE);

        $f = \File::getByID(Config::get('community_store_import.import_file'));
        $fname = $_SERVER['DOCUMENT_ROOT'] . $f->getApprovedVersion()->getRelativePath();

        if (!file_exists($fname) || !is_readable($fname)) {
            $this->error->add(t("Import file not found or is not readable."));
            return;
        }

        if (!$handle = @fopen($fname, 'r')) {
            $this->error->add(t('Cannot open file %s.', $fname));
            return;
        }

        $delim = Config::get('community_store_import.csv.delimiter');
        $delim = ($delim === '\t') ? "\t" : $delim;

        $enclosure = Config::get('community_store_import.csv.enclosure');
        $line_length = Config::get('community_store_import.csv.line_length');

        // Get headings
        $csv = fgetcsv($handle, $line_length, $delim, $enclosure);
        $headings = array_map('strtolower', $csv);

        if ($this->isValid($headings)) {
            $this->error->add(t("Required data missing."));
            return;
        }

        // Get attribute headings
        foreach ($headings as $heading) {
            if (preg_match('/^attr_/', $heading)) {
                $this->attributes[] = $heading;
            }
        }

        $updated = 0;
        $added = 0;

        while (($csv = fgetcsv($handle, $line_length, $delim, $enclosure)) !== FALSE) {
            if (count($csv) === 1) {
                continue;
            }

            // Make associative arrray
            $row = array_combine($headings, $csv);
            if (trim($row['psku']) == '') {
                continue;
            }

            /*setup cIDs*/


//            if ($row['pdetail'] == '' && $row['pdesc'] != '') {
//                $row['pdetail'] = $row['pdesc'];
//                $row['pdesc'] = '';
//            }
//            $row['pdetail'] = '<p>' . $row['pdetail'] . '</p>';
//            $row['cID'] = array();
//            $collection_id = @explode(',', $row['collection_id']);
//            $finish_id = @explode(',', $row['finish_id']);
//            $room_id = @explode(',', $row['room_id']);
//            $row['cID'] = array_map('trim', @array_merge($collection_id, $finish_id, $room_id));
            /*===================*/
            /*setup fID*/
           // $row['attr_product_type'] = array_map('trim', @explode(',', $row['attr_product_type']));
            $row['pProductGroups'] = array_map('trim', @explode(',', $row['pproductgroup_ids']));
            $row['pnoqty'] = 0;
            $row['pqtyunlim'] = true;
            if ($row['image1_id'] != '') {
                $row['pfID'] = $row['image1_id'];
            } else {
                $row['pfID'] = Config::get('community_store_import.default_image');
            }

            $row['pifID'] = array();
            if ($row['image2_id'] != '') {
                $row['pifID'] = array_map('trim', @explode(',', $row['image2_id']));
            }
           /* if ($row['psku'] == 'SOM043') {
                break;
            }*/

            /*================================*/
            $p = Product::getBySKU($row['psku']);
            if ($p instanceof Product) {
               // $this->update($p, $row);
                $updated++;
            } else {
                $p = $this->add($row);
                $added++;
            }
            // @TODO: dispatch events - see Products::save()
        }


        $this->set('success', $this->get('success') . "Import completed: $added products added, $updated products updated.");
        Log::addNotice($this->get('success'));

        ini_set('auto_detect_line_endings', FALSE);
        ini_set('max_execution_time', $MAX_EXECUTION_TIME);
        ini_set('max_input_time', $MAX_INPUT_TIME);
    }

    protected function getCategoryObject()
    {
        return Category::getByHandle('store_product');
    }

    private function setAttributes($product, $row)
    {
        foreach ($this->attributes as $attr) {
            $ak_handle = preg_replace('/^attr_/', '', $attr);
            $ak = $this->getCategoryObject()->getController()->getByHandle($ak_handle);
            if (is_object($ak)) {
                $product->setAttribute($ak, $row[$attr]);
            }
        }
    }

    private function setGroups($product, $row)
    {
        if ($row['pproductgroups']) {
            $pGroupNames = explode(',', $row['pproductgroups']);
            $pGroupIDs = array();
            foreach ($pGroupNames as $pGroupName) {
                $pgID = StoreGroup::getByName(trim($pGroupName));
                if (!$pgID instanceof StoreGroup) {
                    $pgID = StoreGroup::add(trim($pGroupName));
                }
                $pGroupIDs[] = $pgID;
            }
            $data['pProductGroups'] = $pGroupIDs;

            // Update groups
            ProductGroup::addGroupsForProduct($data, $product);
        }
    }

    private function add($row)
    {
	
	$db = \Database::connection();
	
	//get filename and find most recent file version id by using this
	if ($row['image1_id'] != '') {
	    
		$fileSearch = $db->query("SELECT * FROM `FileVersions` WHERE `fvFilename` = ? ORDER BY fvID DESC LIMIT 1", array($row['image1_id']));
		$file = $fileSearch->fetch();
		$fileID = $file['fID'];
		
                $row['pfID'] = $fileID;
		
		//echo $row['image1_id'] .'image name<br/>';
        }
	
	
	
        $data = array(
            'pSKU' => $row['psku'],
            'pName' => $row['pname'],
            'pDesc' => trim($row['pdesc']),
            'pDetail' => trim($row['pdetail']),
            'pCustomerPrice' => $row['pcustomerprice'],
            'pFeatured' => $row['pfeatured'],
            'pQty' => $row['pqty'],
            'pNoQty' => $row['pnoqty'],
            'pTaxable' => $row['ptaxable'],
            'pActive' => $row['pactive'],
            'pShippable' => $row['pshippable'],
            'pCreateUserAccount' => $row['pcreateuseraccount'],
            'pAutoCheckout' => $row['pautocheckout'],
            'pExclusive' => $row['pexclusive'],

            'pPrice' => $row['pprice'],
            'pSalePrice' => $row['psaleprice'],
            'pPriceMaximum' => $row['ppricemaximum'],
            'pPriceMinimum' => $row['ppriceminimum'],
            'pPriceSuggestions' => $row['ppricesuggestions'],
            'pQtyUnlim' => $row['pqtyunlim'],
            'pBackOrder' => $row['pbackorder'],
            'pLength' => $row['plength'],
            'pWidth' => $row['pwidth'],
            'pHeight' => $row['pheight'],
            'pWeight' => $row['pweight'],
            'pNumberItems' => $row['pnumberitems'],

            // CS v1.4.2+
            'pMaxQty' => $row['pmaxqty'],
            'pQtyLabel' => $row['pqtylabel'],
            'pAllowDecimalQty' => (isset($row['pallowdecimalqty']) ? $row['pallowdecimalqty'] : false),
            'pQtySteps' => $row['pqtysteps'],
            'pSeperateShip' => $row['pseperateship'],
            'pPackageData' => $row['ppackagedata'],

            // CS v2+
            'pQtyLabel' => (isset($row['pqtylabel']) ? $row['pqtylabel'] : ''),
            'pMaxQty' => (isset($row['pmaxqty']) ? $row['pmaxqty'] : 0),
            // Not supported in CSV data
            'cID' => $row['cID'],
            'pfID' => $row['pfID'],
            'pifID' => $row['pifID'],
            'pVariations' => true,
            'pQuantityPrice' => false,
            'poName' => 'TEST',
            'pTaxClass' => 1        // 1 = default tax class
        );

        // Save product
        $p = Product::saveProduct($data);
        
        
        
        $productId = $p->getID();
        
        //Get product options in array
        $productOptionsArray = explode(", ", $row['options']);
        
        $sortorder = 0;
        
        foreach($productOptionsArray as $productOption){
          
          //add product option
          
          $optionName = strtolower(str_replace("_", " ", $productOption));
         
          
          $thisOption = ProductOption::add($p, $optionName, $sortorder, $type = 'select', $productOption, false, true, 'select');
	  $p->getOptions()->add($thisOption);
          
          $OptionItemListArray = explode(", ", $row[$optionName]);
          
          $itemSortOrder = 0;
          
          foreach($OptionItemListArray as $optionItem){
            
            $optionItemName = str_replace("_", " ", $optionItem);
            
            $thisOptionItem = ProductOptionItem::add($thisOption, $optionItemName, $itemSortOrder);
	    
	    $thisOption->getOptionItems()->add($thisOptionItem);
            
           
            
            $itemSortOrder++;
          }
          
          
          $sortorder++;
        }
        
        
        $thisorder = 0;
        //get Category Page ID by names
        $categories = explode(", ", $row['category']);
        foreach($categories as $cat){
          $page = \Page::getByPath($cat);
          
          if($page){
            $pageCID = $page->getCollectionID();
            
            if($pageCID != ''){
              echo $pageCID;
            StoreProductLocation::add($p, $pageCID, $thisorder);
            $thisorder++;
            }
          }
        }

        //save category locations
        //StoreProductLocation::addLocationsForProduct($row, $p);

        //save images
        StoreProductImage::addImagesForProduct($row, $p);

        // Add product attributes
        $this->setAttributes($p, $row);

        // Add product groups
        $this->setGroups($p, $row);

        // Add groups
        ProductGroup::addGroupsForProduct($row, $p);
	
	
	//update the variations
	
	ProductVariation::addVariations($data, $p);
	
	$options = $p->getOptions();
	
        $variations = $p->getVariations();

        $variationLookup = [];
        $optionArrays = [];
        $optionLookup = [];
        $optionItems = [];

        foreach ($options as $opt) {
            if ($opt->getIncludeVariations()) {
                $optionLookup[$opt->getID()] = $opt;
		

                foreach ($opt->getOptionItems() as $optItem) {
                    $optionArrays[$opt->getID()][] = $optItem->getID();
                    $optionItemLookup[$optItem->getID()] = $optItem;
                    $optionItems[] = $optItem;
                }
            }
        }

        $this->set('optionItems', $optionItems);
        $this->set('optionLookup', $optionLookup);
        $this->set('optionItemLookup', $optionItemLookup);
	

        $optionArrays = array_values($optionArrays);
	

        $comboOptions = StoreProductVariation::combinations($optionArrays);

        $checkedOptions = [];

        foreach ($comboOptions as $option) {
            if (!is_array($option)) {
                $checkedOptions[] = [$option];
            } else {
                $checkedOptions[] = $option;
            }
        }

        $comboOptions = $checkedOptions;

	
        $this->set('comboOptions', $comboOptions);
        $this->set('optionItemLookup', $optionItemLookup);
	
	$productPrice = $p->getBasePrice();

        foreach ($variations as $variation) {
	    //search variation option items table for items with this variation id
	    $variationID = $variation->getID();
	    $optionPrice = 0;
	    
	   // echo 'variation id = ' . $variationID . '<br/>';
	    
	    $variationComboSearch = $db->query("SELECT * FROM `CommunityStoreProductVariationOptionItems` WHERE `pvID`=?", array($variationID));
	    
	    //get the poiID and use it to find the product option item name, then the price
	    
	    while($variationComboItem = $variationComboSearch->fetch()){
		$productOption = ProductOptionItem::getByID($variationComboItem['poiID']);
		$optionName = $productOption->getName();
		
		//echo 'option item id = ' . $variationComboItem['poiID'] . '<br/>';
		
		$optionNameInRow = str_replace(" ", "_", $optionName);
		$priceNameRow = strtolower($optionNameInRow) . '_price';
		$optionPrice = $optionPrice + $row[$priceNameRow];
		
		//echo 'option price for '.$priceNameRow.' = ' . $row[$priceNameRow] . '<br/>';
	    }
	    
	    //once the price is found, add to the variation price variable, then set the variation price once all are found.
	    $finalVariationPrice = $optionPrice + $productPrice;
	   // echo $finalVariationPrice . ' is final price for ' . $variationID . '<br/><br/>';
	    
	    $db->query("UPDATE `CommunityStoreProductVariations` SET `pvPrice` = ?, `pvQTYUnlim`='1' WHERE `pvID` = ?", array($finalVariationPrice, $variationID));
	    
            $options = $variation->getOptions();
            $optionids = [];

            foreach ($options as $varoption) {
                $option = $varoption->getOption();

                if ($option) {
                    $optionids[] = $option->getID();
                }
            }

            sort($optionids);
            $variationLookup[implode('_', $optionids)] = $variation;
        }

        $this->set('variations', $variations);
        $this->set('variationLookup', $variationLookup);
	

	//$p->save();
	

	
//	
//	$data['pvSKU'] = 'TEstnew';
//	$data['pvPrice'] = 50.00;
	
	
	
        return $p;
    }

    private function update($p, $row)
    {
        if ($row['psku']) $p->setSKU($row['psku']);
        if ($row['pname']) $p->setName($row['pname']);
        if ($row['pdesc']) $p->setDescription($row['pdesc']);
        if ($row['pdetail']) $p->setDetail($row['pdetail']);
        if ($row['pfeatured']) $p->setIsFeatured($row['pfeatured']);
        if ($row['pqty']) $p->setQty($row['pqty']);
        if ($row['pnoqty']) $p->setNoQty($row['pnoqty']);
        if ($row['ptaxable']) $p->setISTaxable($row['ptaxable']);
        if ($row['pactive']) $p->setIsActive($row['pactive']);
        if ($row['pshippable']) $p->setIsShippable($row['pshippable']);
        if ($row['pcreateuseraccount']) $p->setCreatesUserAccount($row['pcreateuseraccount']);
        if ($row['pautocheckout']) $p->setAutoCheckout($row['pautocheckout']);
        if ($row['pexclusive']) $p->setIsExclusive($row['pexclusive']);

        if ($row['pprice']) $p->setPrice($row['pprice']);
        if ($row['psaleprice']) $p->setSalePrice($row['psaleprice']);
        if ($row['ppricemaximum']) $p->setPriceMaximum($row['ppricemaximum']);
        if ($row['ppriceminimum']) $p->setPriceMinimum($row['ppriceminimum']);
        if ($row['ppricesuggestions']) $p->setPriceSuggestions($row['ppricesuggestions']);
        if ($row['pqtyunlim']) $p->setIsUnlimited($row['pqtyunlim']);
        if ($row['pbackorder']) $p->setAllowBackOrder($row['pbackorder']);
        if ($row['plength']) $p->setLength($row['plength']);
        if ($row['pwidth']) $p->setWidth($row['pwidth']);
        if ($row['pheight']) $p->setHeight($row['pheight']);
        if ($row['pweight']) $p->setWeight($row['pweight']);
        if ($row['pnumberitems']) $p->setNumberItems($row['pnumberitems']);

        // CS v1.4.2+
        if ($row['pmaxqty']) $p->setMaxQty($row['pmaxqty']);
        if ($row['pqtylabel']) $p->setQtyLabel($row['pqtylabel']);
        if ($row['pallowdecimalqty']) $p->setAllowDecimalQty($row['pallowdecimalqty']);
        if ($row['pqtysteps']) $p->setQtySteps($row['pqtysteps']);
        if ($row['pseparateship']) $p->setSeparateShip($row['pseparateship']);
        if ($row['ppackagedata']) $p->setPackageData($row['ppackagedata']);
        if (!$p->getImageId())
            $p->setImageId($row['pfID']);

        //save category locations
        StoreProductLocation::addLocationsForProduct($row, $p);

        //save images
        StoreProductImage::addImagesForProduct($row, $p);

        // Product attributes
        $this->setAttributes($p, $row);

        // Product groups
        //$this->setGroups($p, $row);

        $p = $p->save();

        return $p;
    }

    private function saveSettings()
    {
        $data = $this->post();

        // @TODO: Validate post data

        Config::save('community_store_import.import_file', $data['import_file']);
        Config::save('community_store_import.default_image', $data['default_image']);
        Config::save('community_store_import.max_execution_time', $data['max_execution_time']);
        Config::save('community_store_import.csv.delimiter', $data['delimiter']);
        Config::save('community_store_import.csv.enclosure', $data['enclosure']);
        Config::save('community_store_import.csv.line_length', $data['line_length']);
    }

    private function isValid($headings)
    {
        // @TODO: implement

        // @TODO: interrogate database for non-null fields
        $dbname = Config::get('database.connections.concrete.database');

        /*
            SELECT GROUP_CONCAT(column_name) nonnull_columns
            FROM information_schema.columns
            WHERE table_schema = '$dbname'
                AND table_name = 'CommunityStoreProducts'
                AND is_nullable = 'NO'
                // pfID is excluded because it is not-null but also an optional field
                AND column_name not in ('pID', 'pfID', pDateAdded');
        */

        return (false);
    }
}

